function get_requst() {
    // Данные для передачи на сервер допустим id товаров и его количество
    let	t = document.getElementById('input-task-1').value;
// Создаём объект класса XMLHttpRequest
    const request = new XMLHttpRequest();

    /*  Составляем строку запроса и кладем данные, строка состоит из:
    пути до файла обработчика ? имя в GET запросе где будет лежать значение запроса id_product и
    через & мы передаем количество qty_product. */
    const url = "request.php?t=" + t + "&r=1";
    /* Здесь мы указываем параметры соединения с сервером, т.е. мы указываем метод соединения GET,
    а после запятой мы указываем путь к файлу на сервере который будет обрабатывать наш запрос. */
    request.open('GET', url);

// Указываем заголовки для сервера, говорим что тип данных, - контент который мы хотим получить должен быть не закодирован.
    request.setRequestHeader('Content-Type', 'application/x-www-form-url');

// Здесь мы получаем ответ от сервера на запрос, лучше сказать ждем ответ от сервера
    request.addEventListener("readystatechange", () => {

        /*   request.readyState - возвращает текущее состояние объекта XHR(XMLHttpRequest) объекта,
        бывает 4 состояния 4-е состояние запроса - операция полностью завершена, пришел ответ от сервера,
        вот то что нам нужно request.status это статус ответа,
        нам нужен код 200 это нормальный ответ сервера, 401 файл не найден, 500 сервер дал ошибку и прочее...   */
        if (request.readyState === 4 && request.status === 200) {

            // выводим в консоль то что ответил сервер
            console.log( request.responseText );
            //Заменяем содержимое тега <p> в первом примере
            document.getElementById('p-task-1').innerHTML = request.responseText;
        }
    });

// Выполняем запрос
    request.send();
}

function post_requst(){
    let	t = document.getElementById('input-task-2').value;
// Создаем экземпляр класса XMLHttpRequest
    const request = new XMLHttpRequest();

// Указываем путь до файла на сервере, который будет обрабатывать наш запрос
    const url = "request.php";

// Так же как и в GET составляем строку с данными, но уже без пути к файлу
    const params = "t=" + t+ "&r=2";

    /* Указываем что соединение	у нас будет POST, говорим что путь к файлу в переменной url, и что запрос у нас
    асинхронный, по умолчанию так и есть не стоит его указывать, еще есть 4-й параметр пароль авторизации, но этот
        параметр тоже необязателен.*/
    request.open("POST", url, true);

//В заголовке говорим что тип передаваемых данных закодирован.
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    request.addEventListener("readystatechange", () => {

        if(request.readyState === 4 && request.status === 200) {
            console.log(request.responseText);
            //Заменяем содержимое тега <p> во втором примере
            document.getElementById('p-task-2').innerHTML = request.responseText;
        }
    });

//	Вот здесь мы и передаем строку с данными, которую формировали выше. И собственно выполняем запрос.
    request.send(params);
}